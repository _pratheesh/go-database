package database

import (
	"fmt"

	"gitlab.com/_pratheesh/go-database/adapter"
	"gitlab.com/_pratheesh/go-database/go_orm"
	"gitlab.com/_pratheesh/go-database/mongo"
	"gitlab.com/_pratheesh/go-database/mysql"
	"gitlab.com/_pratheesh/go-database/postgres"
)

type DBO struct {
	Adapter adapter.AdapterInterface
}

/**
 * --------------------------------------------------------------------------------
 * Adapter object inititation
 * --------------------------------------------------------------------------------
 */
func (db *DBO) DBAdapter(adapter string) {

	if adapter == "mysql" {
		mysqlAdpObj := &mysql.MySqlAdapter{}
		db.Adapter = mysqlAdpObj
	} else if adapter == "postgres" {
		psqlAdpObj := &postgres.PostgresAdapter{}
		db.Adapter = psqlAdpObj
	} else if adapter == "mongo" {
		mongoAdpObj := &mongo.MongoAdapter{}
		db.Adapter = mongoAdpObj
	} else if adapter == "orm_mysql" || adapter == "orm_postgres" {
		ormAdpObj := &go_orm.ORMAdapter{}
		db.Adapter = ormAdpObj
	} else {
		panic(fmt.Sprintf("Unable to find database adapter %v", adapter))
	}

}
