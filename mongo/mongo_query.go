package mongo

import (
	"context"
	"fmt"
	"log"

	"gitlab.com/_pratheesh/go-database/adapter"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"gopkg.in/mgo.v2/bson"
)

type Query struct {
	CollectionClient *mongo.Collection
	Cursor           *mongo.Cursor

	Condition bson.M
	Fields    bson.M
	Options   *options.FindOptions
}

var (
	ctx = context.Background()
)

/**
 * ----------------------------------------------------------------------
 * Clear querys
 *
 * Initiate query with null values
 * ----------------------------------------------------------------------
 */
func (mongoadapter *MongoAdapter) QueryClose() {
	defer mongoadapter.Cursor.Close(ctx)

}

/**
 * ----------------------------------------------------------------------
 * Initiate query
 *
 * Initiate query with null values
 * ----------------------------------------------------------------------
 */
func (mongoadapter *MongoAdapter) QueryInit() adapter.QueryInterface {
	var qinterface adapter.QueryInterface
	qinterface = mongoadapter

	mongoadapter.Query.Options = options.Find()

	return qinterface
}

/**
 *
 * Connect to cllection
 * Create a handle to the respective collection in the database.
 */
func (mongoadapter *MongoAdapter) ConnectCollection(tb string) *mongo.Collection {
	collection := mongoadapter.DBConn.Database(mongoadapter.Database).Collection(tb)
	return collection
}

/**
 * ----------------------------------------------------------------------
 * Log queries
 *
 * ----------------------------------------------------------------------
 */
func (mongoadapter *MongoAdapter) QueryLog(query_stment string, values ...interface{}) {
	if mongoadapter.DBLOG {
		log.Println("[Query:]", query_stment, "[values:]", values)
	}
}

func (mongoadapter *MongoAdapter) Select(table string, alias string) {
	mongoadapter.Query.CollectionClient = mongoadapter.ConnectCollection(table)
}

/**
 * ----------------------------------------------------------------------
 * Append the fields
 * Select fields
 * ----------------------------------------------------------------------
 */
func (mongoadapter *MongoAdapter) Fields(fields ...string) {
}
func (mongoadapter *MongoAdapter) Condition(cond string, conditionmap ...interface{}) {
	if len(conditionmap) > 0 {
		mongoadapter.Query.Condition = conditionmap[0].(bson.M)
	}
}

/**
 * ----------------------------------------------------------------------
 * Append order by clause
 *
 * ----------------------------------------------------------------------
 */
func (mongoadapter *MongoAdapter) OrderBy(order_field string, directions string) {
	// order_i := 1
	// if directions == "DESC" {
	// 	order_i = -1
	// }

	// mongoadapter.Query.Options.SetSort(bson.D{{"data", -1}})

}

/**
 * ----------------------------------------------------------------------
 * Append Group By Clause
 *
 * ----------------------------------------------------------------------
 */
func (mongoadapter *MongoAdapter) GroupBy(group_fields ...string) {
}

/**
 * ----------------------------------------------------------------------
 * Append Limit Clause
 *
 * ----------------------------------------------------------------------
 */
func (mongoadapter *MongoAdapter) Limit(limit ...int) {
	if len(limit) == 1 {
		mongoadapter.Query.Options.SetLimit(int64(limit[0]))
	}

	if len(limit) > 1 {
		mongoadapter.Query.Options.SetSkip(int64(limit[0]))  // skip whatever you want, like `offset` clause in mysql
		mongoadapter.Query.Options.SetLimit(int64(limit[1])) // like `limit` clause in mysql
	}

}

/**
 * ----------------------------------------------------------------------
 * Prepare Statement
 *
 * Prepare Queries
 * ----------------------------------------------------------------------
 */
func (mongoadapter *MongoAdapter) Prepare() {

}

/**
 * ----------------------------------------------------------------------
 * Fetch Assoc
 *
 * ----------------------------------------------------------------------
 */
func (mongoadapter *MongoAdapter) FetchAssoc(values ...interface{}) (resultData map[string]string, err error) {
	resultData = make(map[string]string)

	// Finding multiple documents returns a cursor
	// Iterating through the cursor allows us to decode documents one at a time
	// Perform Find operation & validate against the error.
	var documents bson.M
	if err = mongoadapter.CollectionClient.FindOne(ctx, bson.M{}).Decode(&documents); err != nil {
		log.Fatal(err)
	}

	for j, doc := range documents {
		switch doc.(type) {
		case primitive.ObjectID:
			mongoId := documents["_id"]
			stringObjectID := mongoId.(primitive.ObjectID).Hex()
			resultData[j] = stringObjectID
		default:
			resultData[j] = fmt.Sprintf("%v", doc)
		}

	}

	return
}

/**
 * ----------------------------------------------------------------------
 * Fetch AAllssoc
 *
 * ----------------------------------------------------------------------
 */
func (mongoadapter *MongoAdapter) FetchAll(values ...interface{}) (resultData map[int]map[string]string, err error) {
	resultData = make(map[int]map[string]string)

	// Passing bson.D{{}} as the filter matches all documents in the collection
	cur, err := mongoadapter.CollectionClient.Find(ctx, mongoadapter.Query.Condition, mongoadapter.Query.Options)
	if err != nil {
		log.Fatal(err)
	}

	// Finding multiple documents returns a cursor
	// Iterating through the cursor allows us to decode documents one at a time
	// if err = cur.All(ctx, &documents); err != nil {
	// 	log.Fatal(err)
	// }

	var j int
	var documents bson.M
	for cur.Next(ctx) {
		// call cur.Decode()
		err := cur.Decode(&documents)
		if err != nil {
			log.Fatal(err)
		}
		resultData[j] = make(map[string]string)

		for i, raw := range documents {
			if raw == nil {
				resultData[j][i] = ""
			} else {
				switch raw.(type) {
				case primitive.ObjectID:
					mongoId := raw
					stringObjectID := mongoId.(primitive.ObjectID).Hex()
					resultData[j][i] = stringObjectID
				default:
					resultData[j][i] = fmt.Sprintf("%v", raw)

				}
			}

		}
		j++

	}
	// once exhausted, close the cursor
	cur.Close(ctx)
	return
}

/**
 * ----------------------------------------------------------------------
 * Fetch Col
 *
 * ----------------------------------------------------------------------
 */
func (mongoadapter *MongoAdapter) FetchCol(values ...interface{}) (resultData []string, errR error) {
	// Passing bson.D{{}} as the filter matches all documents in the collection
	cur, err := mongoadapter.CollectionClient.Find(ctx, mongoadapter.Query.Condition, mongoadapter.Query.Options)
	if err != nil {
		log.Fatal(err)
	}

	var keyname, keyVal string
	if len(values) > 0 {
		keyname = values[0].(string)
	} else {
		keyname = "_id"
	}

	var documents bson.M
	for cur.Next(ctx) {
		// call cur.Decode()
		err := cur.Decode(&documents)
		if err != nil {
			log.Fatal(err)
		}

		switch documents[keyname].(type) {
		case primitive.ObjectID:
			mongoId := documents[keyname]
			keyVal = mongoId.(primitive.ObjectID).Hex()
		default:
			keyVal = documents[keyname].(string)
		}
		resultData = append(resultData, keyVal)
	}
	return
}

/**
 * ----------------------------------------------------------------------
 * Fetch all Keyed
 *
 * ----------------------------------------------------------------------
 */
func (mongoadapter *MongoAdapter) FetchAllKeyed(field string, values ...interface{}) (resultData map[int]map[string]string, errR error) {
	// Passing bson.D{{}} as the filter matches all documents in the collection
	cur, err := mongoadapter.CollectionClient.Find(ctx, mongoadapter.Query.Condition, mongoadapter.Query.Options)
	if err != nil {
		log.Fatal(err)
	}

	var documents bson.M
	for cur.Next(ctx) {
		// call cur.Decode()
		err := cur.Decode(&documents)
		if err != nil {
			log.Fatal(err)
		}
	}
	return
}

/**
 * ----------------------------------------------------------------------
 * Fetch count
 *
 * ----------------------------------------------------------------------
 */
func (mongoadapter *MongoAdapter) FetchCount(values ...interface{}) (count int, errR error) {
	itemCount, err := mongoadapter.CollectionClient.CountDocuments(ctx, mongoadapter.Query.Condition)
	if err != nil {
		log.Fatal(err)
	}

	count = int(itemCount)
	return

}

/**
 * ----------------------------------------------------------------------
 * Insert fields
 *
 * ----------------------------------------------------------------------
 */
func (mongoadapter *MongoAdapter) Insert(tb string, fields map[string]interface{}) (status bool, err error) {

	_, err = mongoadapter.ConnectCollection(tb).InsertOne(ctx, fields)
	if err == nil {
		status = true
	}
	return
}

/**
 * ----------------------------------------------------------------------
 * Multi Insert fields
 *
 * ----------------------------------------------------------------------
 */

func (mongoadapter *MongoAdapter) MultiInsert(tb string, fields map[int]map[string]interface{}) (status bool, err error) {
	var fieldsInterface []interface{}

	for _, fld := range fields {
		fieldsInterface = append(fieldsInterface, fld)
	}

	if len(fieldsInterface) > 0 {
		_, err = mongoadapter.ConnectCollection(tb).InsertMany(ctx, fieldsInterface)
		if err == nil {
			status = true
		}
	}

	return
}

/**
 * ----------------------------------------------------------------------
 * Update
 * ----------------------------------------------------------------------
 */
func (mongoadapter *MongoAdapter) Update(tb string, fields map[string]interface{}, cond string, expr string) (status bool, err error) {
	_, err = mongoadapter.ConnectCollection(tb).UpdateMany(ctx, mongoadapter.Query.Condition, fields)
	if err == nil {
		status = true
	}

	return
}

/**
 * ----------------------------------------------------------------------
 * Delete
 * ----------------------------------------------------------------------
 */
func (mongoadapter *MongoAdapter) Delete(tb string, fields map[string]interface{}, cond string) (status bool, err error) {
	_, err = mongoadapter.ConnectCollection(tb).DeleteMany(ctx, fields)
	if err == nil {
		status = true
	}

	return
}
