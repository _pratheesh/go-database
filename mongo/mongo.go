package mongo

import (
	"context"
	"fmt"

	"gitlab.com/_pratheesh/go-database/config"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type MongoAdapter struct {
	Host, Port, User, Pass string
	Database               string
	Domine                 string

	DBConn *mongo.Client
	Query
	DBLOG bool
}

//
// LoadConfig
//
// Initiate the connection variables
func (db *MongoAdapter) LoadConfig(configVal config.DBConfig) {
	db.Host = configVal.Host
	db.User = configVal.User
	db.Pass = configVal.Pass
	db.Port = configVal.Port
	db.Database = configVal.Database
	db.Domine = configVal.Domine
}

//
// Close connection
//
func (db *MongoAdapter) DBClose() {
	//db.DBConn.Close()
}

//
// Initiate database connection
//
func (db *MongoAdapter) DBConnection() {

	// Set client options
	clientOptions := options.Client().ApplyURI(fmt.Sprintf("mongodb://%v:%v", db.Host, db.Port))

	// Connect to MongoDB
	client, err := mongo.Connect(context.TODO(), clientOptions)
	if err != nil {
		panic(err)
	}

	// Check the connection
	err = client.Ping(context.TODO(), nil)

	if err != nil {
		panic(err)
	}

	db.DBConn = client
}

//
// Connection object
//
func (mongoadapter *MongoAdapter) Connection() {
	fmt.Println("Create functon called")
}

func (mongoadapter *MongoAdapter) Create() {
	fmt.Println("Create functon called")
}
