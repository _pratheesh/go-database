package adapter

import "gitlab.com/_pratheesh/go-database/config"

type AdapterInterface interface {
	LoadConfig(config.DBConfig)
	DBConnection()
	DBClose()

	QueryInit() QueryInterface
	QueryClose()
	QueryLog(string, ...interface{})

	Connection()
	Create()
}

type QueryInterface interface {
	Select(string, string)
	Fields(...string)
	Condition(string, ...interface{})
	OrderBy(string, string)
	GroupBy(...string)
	Limit(...int)
	Prepare()

	FetchAssoc(...interface{}) (map[string]string, error)
	FetchAll(...interface{}) (map[int]map[string]string, error)
	FetchCol(...interface{}) ([]string, error)
	FetchAllKeyed(string, ...interface{}) (map[int]map[string]string, error)
	FetchCount(...interface{}) (int, error)

	Insert(string, map[string]interface{}) (bool, error)
	MultiInsert(string, map[int]map[string]interface{}) (bool, error)

	Update(string, map[string]interface{}, string, string) (bool, error)
	Delete(string, map[string]interface{}, string) (bool, error)
}
