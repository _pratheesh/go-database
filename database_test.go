package database_test

import (
	"testing"

	"gitlab.com/_pratheesh/go-database/config"

	"gitlab.com/_pratheesh/go-database"
)

func TestConnection(t *testing.T) {
	connObje := database.DBO{}
	connObje.DBAdapter("mongo")
	connObje.Adapter.LoadConfig(config.DBConfig{
		Domine:   "mongo",
		Host:     "localhost",
		Port:     "27017",
		User:     "postgres",
		Pass:     "postgres",
		Database: "copier_database",
	})
	connObje.Adapter.DBConnection()

	connObje.Adapter.Connection()

	// ORMDBC := connObje.Adapter.(*go_orm.ORMAdapter).DB
	// fmt.Printf("%T\n", ORMDBC)

	// type User struct {
	// 	Name string
	// 	Age  int
	// }

	// // Migrate the schema
	// ORMDBC.AutoMigrate(&User{})

	// user := User{Name: "Jinzhu", Age: 18}

	// result := ORMDBC.Create(&user) // pass pointer of data to Create

	// fmt.Println(result)

	query := connObje.Adapter.QueryInit()
	defer connObje.Adapter.DBClose()

	t.Log(query)

	// query.Select("request_list_tables", "alias")
	// query.Condition("", bson.M{
	// 	"no": bson.M{
	// 		"$gt": 0,
	// 	},
	// })
	// query.OrderBy("data", "DESC")
	// query.Limit(10)
	// query.Prepare()
	// data, err := query.FetchCount()
	// t.Log(err)
	// t.Log(data)
	// assert.NoError(t, err)

}
