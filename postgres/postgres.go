package postgres

import (
	"database/sql"
	"fmt"
	"log"
	"time"

	"gitlab.com/_pratheesh/go-database/adapter"
	"gitlab.com/_pratheesh/go-database/config"
	"gitlab.com/_pratheesh/go-database/query"

	_ "github.com/lib/pq"
)

type PostgresAdapter struct {
	DBConn                 *sql.DB
	Host, Port, User, Pass string
	Database               string
	Domine                 string
	Sslmode                string
	query.Query
	DBLOG bool
}

//
// LoadConfig
//
// Initiate the connection variables
func (db *PostgresAdapter) LoadConfig(configVal config.DBConfig) {
	db.Host = configVal.Host
	db.User = configVal.User
	db.Pass = configVal.Pass
	db.Port = configVal.Port
	db.Database = configVal.Database
	db.Domine = configVal.Domine
	db.Sslmode = configVal.Sslmode
}

//
// Close connection
//
func (db *PostgresAdapter) DBClose() {
	db.DBConn.Close()
}

//
// Initiate database connection
//
func (db *PostgresAdapter) DBConnection() {

	var connectiondb *sql.DB
	var err error
	postgresInfo := fmt.Sprintf("host=%s port=%s user=%s "+
		"password=%s dbname=%s sslmode=%s extra_float_digits=2",
		db.Host, db.Port, db.User, db.Pass, db.Database, db.Sslmode)

	connectiondb, err = sql.Open("postgres", postgresInfo)

	connectiondb.SetMaxOpenConns(5)
	connectiondb.SetMaxIdleConns(3)
	connectiondb.SetConnMaxLifetime(time.Second * 1)

	fmt.Println(connectiondb)

	if err != nil {
		panic(fmt.Sprintf("Postgres connection error %v", err))
	}
	err = connectiondb.Ping()
	if err != nil {
		panic(fmt.Sprintf("Postgres ping error %v", err))
	}

	fmt.Println("Successfully connected!")
	db.DBConn = connectiondb
}

//
// Connection object
//
func (PostgresAdapter *PostgresAdapter) Connection() {
	fmt.Println("Create functon called")
}

func (PostgresAdapter *PostgresAdapter) Create() {
	fmt.Printf("%+v", PostgresAdapter)
	fmt.Println("Create functon called")
}

/**
 * ----------------------------------------------------------------------
 * Initiate query
 *
 * Initiate query with null values
 * ----------------------------------------------------------------------
 */
func (dbI *PostgresAdapter) QueryInit() adapter.QueryInterface {
	q := &dbI.Query
	q.Qstmnt = ""
	q.SelectQ = query.SelectQ{}
	q.SelectQ.Select = ""
	q.SelectQ.Where = ""
	q.SelectQ.From = ""
	q.SelectQ.Limit = ""
	q.SelectQ.Order = ""
	q.SelectQ.Join = ""
	q.SelectQ.Group = ""

	q.Connection = dbI.DBConn
	q.QueryClose = dbI.QueryClose
	q.QueryLog = dbI.QueryLog
	q.Domine = "postgres"

	var qinterface adapter.QueryInterface
	qinterface = q
	return qinterface
}

/**
 * ----------------------------------------------------------------------
 * Clear querys
 *
 * Initiate query with null values
 * ----------------------------------------------------------------------
 */
func (dbI *PostgresAdapter) QueryClose() {
	dbI.Query.Qstmnt = ""
	dbI.Query.SelectQ = query.SelectQ{}
	dbI.Query.SelectQ.Select = ""
	dbI.Query.SelectQ.Where = ""
	dbI.Query.SelectQ.From = ""
	dbI.Query.SelectQ.Limit = ""
	dbI.Query.SelectQ.Order = ""
	dbI.Query.SelectQ.Group = ""
	dbI.Query.SelectQ.Join = ""
}

/**
 * ----------------------------------------------------------------------
 * Log queries
 *
 * ----------------------------------------------------------------------
 */
func (dbI *PostgresAdapter) QueryLog(query_stment string, values ...interface{}) {
	if dbI.DBLOG {
		log.Println("[Query:]", query_stment, "[values:]", values)
	}
}
