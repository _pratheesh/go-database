package mysql

import (
	"database/sql"
	"fmt"
	"log"

	_ "github.com/go-sql-driver/mysql"

	"gitlab.com/_pratheesh/go-database/adapter"
	"gitlab.com/_pratheesh/go-database/config"
	"gitlab.com/_pratheesh/go-database/query"
)

type MySqlAdapter struct {
	DBConn                 *sql.DB
	Host, Port, User, Pass string
	Database               string
	Domine                 string

	query.Query
	DBLOG bool
}

//
// LoadConfig
//
// Initiate the connection variables
func (db *MySqlAdapter) LoadConfig(configVal config.DBConfig) {
	db.Host = configVal.Host
	db.User = configVal.User
	db.Pass = configVal.Pass
	db.Port = configVal.Port
	db.Database = configVal.Database
	db.Domine = configVal.Domine
}

//
// Close connection
//
func (db *MySqlAdapter) DBClose() {
	db.DBConn.Close()
}

//
// Initiate database connection
//
func (db *MySqlAdapter) DBConnection() {

	var connectiondb *sql.DB
	var err error
	mysqlInfo := fmt.Sprintf("%s:%s@tcp(%s)/%s", db.User, db.Pass, db.Host, db.Database)
	connectiondb, err = sql.Open("mysql", mysqlInfo)

	if err != nil {
		panic(err)
	}

	err = connectiondb.Ping()
	if err != nil {
		panic(err)
	}

	fmt.Println("Successfully connected!")
	db.DBConn = connectiondb
}

//
// Connection object
//
func (mysqladapter *MySqlAdapter) Connection() {
	fmt.Println("Create functon called")
}

func (mysqladapter *MySqlAdapter) Create() {
	fmt.Printf("%+v", mysqladapter)
	fmt.Println("Create functon called")
}

/**
 * ----------------------------------------------------------------------
 * Initiate query
 *
 * Initiate query with null values
 * ----------------------------------------------------------------------
 */
func (dbI *MySqlAdapter) QueryInit() adapter.QueryInterface {
	q := &dbI.Query
	q.Qstmnt = ""
	q.SelectQ = query.SelectQ{}
	q.SelectQ.Select = ""
	q.SelectQ.Where = ""
	q.SelectQ.From = ""
	q.SelectQ.Limit = ""
	q.SelectQ.Order = ""
	q.SelectQ.Join = ""
	q.SelectQ.Group = ""

	q.Connection = dbI.DBConn
	q.QueryClose = dbI.QueryClose
	q.QueryLog = dbI.QueryLog
	q.Domine = "mysql"

	var qinterface adapter.QueryInterface
	qinterface = q
	return qinterface
}

/**
 * ----------------------------------------------------------------------
 * Clear querys
 *
 * Initiate query with null values
 * ----------------------------------------------------------------------
 */
func (dbI *MySqlAdapter) QueryClose() {
	dbI.Query.Qstmnt = ""
	dbI.Query.SelectQ = query.SelectQ{}
	dbI.Query.SelectQ.Select = ""
	dbI.Query.SelectQ.Where = ""
	dbI.Query.SelectQ.From = ""
	dbI.Query.SelectQ.Limit = ""
	dbI.Query.SelectQ.Order = ""
	dbI.Query.SelectQ.Group = ""
	dbI.Query.SelectQ.Join = ""
}

/**
 * ----------------------------------------------------------------------
 * Log queries
 *
 * ----------------------------------------------------------------------
 */
func (dbI *MySqlAdapter) QueryLog(query_stment string, values ...interface{}) {
	if dbI.DBLOG {
		log.Println("[Query:]", query_stment, "[values:]", values)
	}
}
