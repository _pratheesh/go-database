package config

type DBConfig struct {
	Domine   string
	Host     string
	Port     string
	User     string
	Pass     string
	Database string
	Sslmode  string
}
