# GO-DATABSE

## Introdution
This a plugin that is used to connect the data bases with native way

## Methods included
- 


## Connection

define object
```
	connObje := database.DBO{}
```

this will define the configuration
```
config := config.DBConfig{
    Domine:   "mongo",
    Host:     "localhost",
    Port:     "27017",
    User:     "postgres",
    Pass:     "postgres",
    Database: "copier_database",
}
```

Initiate the adapter with the sql domine
```
	connObje.DBAdapter(config.Domine)
```

Initiate the configs
```
	connObje.Adapter.LoadConfig(config)
```

Create connection
```
	connObje.Adapter.DBConnection()
```
at this point the connection is initiated and we can query the tables


sample query

Initiating the query object
select query
```
	query := connObje.Adapter.QueryInit()
	defer connObje.Adapter.DBClose()

    query.Select("request_list_tables", "alias")
	query.Condition("", bson.M{
	 	"no": bson.M{
			"$gt": 0,
		},
	})
	query.OrderBy("data", "DESC")
	query.Limit(10)
	query.Prepare()
	data, err := query.FetchCount()


	fmt.Println(data)
	fmt.Println(err)
```

Insert
```
    data, err := query.Insert("request_list_tables", map[string]interface{}{
		"file_path": "'file_path1'",
	})


```

Multi Insert

```
data, err := query.MultiInsert("request_list_tables", map[int]map[string]interface{}{
		0: map[string]interface{}{
			"file_path": "'file_path1'",
		},
		1: map[string]interface{}{
			"file_path": "'file_path2'",
		},
	})
```


Config sample
- postgres
	```
	config.DBConfig{
		Domine:   "postgres",
		Host:     "localhost",
		Port:     "5432",
		User:     "postgres",
		Pass:     "postgres",
		Database: "copier_database",
		Sslmode:  "disable",
	}
	```
- mysql
	```
	config.DBConfig{
		Domine:   "mysql",
		Host:     "localhost",
		User:     "root",
		Pass:     "root",
		Database: "copier_database",
	}
	```
- orm_mysql
	```
	config.DBConfig{
		Domine:   "orm_mysql",
		Host:     "localhost",
		Port:     "3306",
		User:     "root",
		Pass:     "root",
		Database: "copier_database",
	}
	```