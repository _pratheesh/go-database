package query

import (
	"database/sql"
	"errors"
	"fmt"
	"sort"
	"strconv"
	"strings"
)

type SelectQ struct {
	Select string
	From   string
	Where  string
	Limit  string
	Order  string
	Group  string
	Join   string
}

type Query struct {
	Connection *sql.DB
	SelectQ
	Qstmnt string

	Domine     string
	QueryError error
	QueryLog   func(string, ...interface{})
	QueryClose func()
}

func (query *Query) Close() {
	query.Connection.Close()
}

/**
 * ----------------------------------------------------------------------
 * Select query
 * ----------------------------------------------------------------------
 */
func (query *Query) Select(table string, alias string) {
	query.Qstmnt = ""
	query.SelectQ = SelectQ{}
	if table == "" {
		query.QueryError = errors.New("Table name not found")
	}
	if query.QueryError != nil {
		panic(query.QueryError)
	}
	if alias == "" {
		alias = table
	}
	query.SelectQ.From = " FROM " + table + " AS " + alias + " "
}

/**
 * ----------------------------------------------------------------------
 * Append the fields
 * Select fields
 * ----------------------------------------------------------------------
 */
func (query *Query) Fields(fields ...string) {
	query.Qstmnt = ""
	if len(fields) <= 0 {
		query.QueryError = errors.New("Fields not found")
	}
	if query.QueryError != nil {
		panic(query.QueryError)
	}
	query.SelectQ.Select = " SELECT " + strings.Join(fields, ",") + " "
}

/**
 * ----------------------------------------------------------------------
 * Join query append
 *
 * ----------------------------------------------------------------------
 */
func (q *Query) Join(join string) {
	q.Qstmnt = ""
	if q.QueryError != nil {
		panic(q.QueryError)
	}
	q.SelectQ.Join += " " + join + " "
}

/**
 * ----------------------------------------------------------------------
 * Add Condition to query
 *
 * ----------------------------------------------------------------------
 */
func (q *Query) Condition(cond string, conditionmap ...interface{}) {
	q.Qstmnt = ""
	if q.QueryError != nil {
		panic(q.QueryError)
	}

	if q.SelectQ.Where != "" {
		q.SelectQ.Where += " AND "
	}
	q.SelectQ.Where += " " + cond + " "
}

/**
 * ----------------------------------------------------------------------
 * Append order by clause
 *
 * ----------------------------------------------------------------------
 */
func (q *Query) OrderBy(order_field string, directions string) {
	q.Qstmnt = ""
	if q.QueryError != nil {
		panic(q.QueryError)
	}

	if q.SelectQ.Order != "" {
		q.SelectQ.Order += " , "
	}

	q.SelectQ.Order += " " + order_field + " " + directions + " "
}

/**
 * ----------------------------------------------------------------------
 * Append Group By Clause
 *
 * ----------------------------------------------------------------------
 */
func (q *Query) GroupBy(group_fields ...string) {
	q.Qstmnt = ""
	if q.QueryError != nil {
		panic(q.QueryError)
	}
	q.SelectQ.Group += " " + strings.Join(group_fields, ",") + " "
}

/**
 * ----------------------------------------------------------------------
 * Append Limit Clause
 *
 * ----------------------------------------------------------------------
 */
func (q *Query) Limit(limit ...int) {
	q.Qstmnt = ""
	if q.QueryError != nil {
		panic(q.QueryError)
	}
	if len(limit) == 1 {
		q.SelectQ.Limit = fmt.Sprintf(" LIMIT %d ", limit[0])
	} else if len(limit) > 1 {
		q.SelectQ.Limit = fmt.Sprintf(" LIMIT %d,%d ", limit[0], limit[1])
	}

}

/**
 * ----------------------------------------------------------------------
 * Prepare Statement
 *
 * Prepare Queries
 * ----------------------------------------------------------------------
 */
func (q *Query) Prepare() {
	if q.QueryError != nil {
		panic(q.QueryError)
	}
	if q.SelectQ.Select == "" {
		q.QueryError = errors.New("Fields not found")
		panic(q.QueryError)
	}
	if q.SelectQ.From == "" {
		q.QueryError = errors.New("Table name not found")
		panic(q.QueryError)
	}
	q.Qstmnt = q.SelectQ.Select + q.SelectQ.From

	if q.SelectQ.Join != "" {
		q.Qstmnt = q.SelectQ.Select + q.SelectQ.From + q.SelectQ.Join
	}

	if q.SelectQ.Where != "" {
		q.Qstmnt += " WHERE " + q.SelectQ.Where + " "
	}

	if q.SelectQ.Group != "" {
		q.Qstmnt += " GROUP BY " + q.SelectQ.Group + " "
	}

	if q.SelectQ.Order != "" {
		q.Qstmnt += " ORDER BY " + q.SelectQ.Order + " "
	}

	q.Qstmnt += q.SelectQ.Limit
}

/**
 * ----------------------------------------------------------------------
 * Fetch Assoc
 *
 * ----------------------------------------------------------------------
 */
func (dbI *Query) FetchAssoc(values ...interface{}) (rData map[string]string, errR error) {
	//check transaction and get connection
	_connectionObject := dbI.Connection

	if dbI.Qstmnt == "" {
		return make(map[string]string, 0), errors.New("SQL statement is missing")
	}

	dbI.QueryLog(dbI.Qstmnt, values)
	rows, err := _connectionObject.Query(dbI.Qstmnt, values...)
	dbI.QueryClose()

	if err != nil {
		return make(map[string]string, 0), err
	}

	cols, err := rows.Columns()

	if err != nil {
		return make(map[string]string, 0), err
	}

	// Result is your slice string.
	rawResult := make([][]byte, len(cols))

	rData = make(map[string]string, len(cols))

	dest := make([]interface{}, len(cols)) // A temporary interface{} slice
	for i := range rawResult {
		dest[i] = &rawResult[i] // Put pointers to each string in the interface slice
	}

	for rows.Next() {
		err = rows.Scan(dest...)
		if err != nil {
			return make(map[string]string, 0), err
		}

		for i, raw := range rawResult {
			if raw == nil {
				rData[cols[i]] = ""
			} else {
				rData[cols[i]] = string(raw)
			}
		}
	}

	return rData, nil

}

/**
 * ----------------------------------------------------------------------
 * Fetch AAllssoc
 *
 * ----------------------------------------------------------------------
 */
func (query *Query) FetchAll(values ...interface{}) (rData map[int]map[string]string, errR error) {

	//check transaction and get connection
	_connectionObject := query.Connection

	if query.Qstmnt == "" {
		return make(map[int]map[string]string, 0), errors.New("SQL statement is missing")
	}

	query.QueryLog(query.Qstmnt, values)
	rows, err := _connectionObject.Query(query.Qstmnt, values...)
	query.QueryClose()

	if err != nil {
		return make(map[int]map[string]string, 0), err
	}

	cols, err := rows.Columns()

	if err != nil {
		return make(map[int]map[string]string, 0), err
	}

	// Result is your slice string.
	rawResult := make([][]byte, len(cols))

	result := make(map[int]map[string]string)

	dest := make([]interface{}, len(cols)) // A temporary interface{} slice
	for i := range rawResult {
		dest[i] = &rawResult[i] // Put pointers to each string in the interface slice
	}
	var j int
	for rows.Next() {
		err = rows.Scan(dest...)
		if err != nil {
			return make(map[int]map[string]string, 0), err
		}
		j++
		result[j] = make(map[string]string)
		for i, raw := range rawResult {

			if raw == nil {
				result[j][cols[i]] = ""
			} else {
				result[j][cols[i]] = string(raw)
			}
		}
	}

	return result, nil

}

/**
 * ----------------------------------------------------------------------
 * Fetch Col
 *
 * ----------------------------------------------------------------------
 */
func (query *Query) FetchCol(values ...interface{}) (rData []string, errR error) {

	//check transaction and get connection
	_connectionObject := query.Connection

	if query.Qstmnt == "" {
		return make([]string, 0), errors.New("SQL statement is missing")
	}

	query.QueryLog(query.Qstmnt, values)

	rows, err := _connectionObject.Query(query.Qstmnt, values...)
	query.QueryClose()

	var fieldD string

	if err != nil {
		return make([]string, 0), err
	}

	// Result is your slice string.
	var j int
	for rows.Next() {
		err = rows.Scan(&fieldD)
		if err != nil {
			return make([]string, 0), err
		}
		j++
		rData = append(rData, fieldD)

	}
	return rData, nil

}

/**
 * ----------------------------------------------------------------------
 * Fetch CoAll Keyedl
 *
 * ----------------------------------------------------------------------
 */
func (query *Query) FetchAllKeyed(field string, values ...interface{}) (rData map[int]map[string]string, errR error) {
	//check transaction and get connection
	_connectionObject := query.Connection

	if query.Qstmnt == "" {
		return make(map[int]map[string]string, 0), errors.New("SQL statement is missing")
	}

	query.QueryLog(query.Qstmnt, values)
	rows, err := _connectionObject.Query(query.Qstmnt, values...)
	query.QueryClose()

	if err != nil {
		return make(map[int]map[string]string, 0), err
	}

	cols, err := rows.Columns()

	if err != nil {
		return make(map[int]map[string]string, 0), err
	}

	// Result is your slice string.
	rawResult := make([][]byte, len(cols))

	result := make(map[int]map[string]string)

	dest := make([]interface{}, len(cols)) // A temporary interface{} slice
	for i := range rawResult {
		dest[i] = &rawResult[i] // Put pointers to each string in the interface slice
	}
	var j int
	for rows.Next() {
		err = rows.Scan(dest...)
		if err != nil {
			return make(map[int]map[string]string, 0), err
		}
		var index_key int
		for k, col_val := range cols {
			if col_val == field {
				index_key = k
				break
			}
		}

		j++
		key_indexStr := string(rawResult[index_key])
		keyIndex, _ := strconv.Atoi(key_indexStr)
		result[keyIndex] = make(map[string]string)
		for i, raw := range rawResult {
			if raw == nil {
				result[keyIndex][cols[i]] = ""
			} else {
				result[keyIndex][cols[i]] = string(raw)
			}
		}
	}

	return result, nil

}

/**
 * ----------------------------------------------------------------------
 * Fetch count
 *
 * ----------------------------------------------------------------------
 */
func (query *Query) FetchCount(values ...interface{}) (count int, errR error) {
	//check transaction and get connection
	_connectionObject := query.Connection

	if query.Qstmnt == "" {
		return 0, errors.New("SQL statement is missing")
	}

	query.QueryLog(query.Qstmnt, values)

	rows, err := _connectionObject.Query(query.Qstmnt, values...)
	query.QueryClose()
	if err != nil {
		return 0, err
	}

	for rows.Next() {
		errR = rows.Scan(&count)
		if errR != nil {
			return 0, errR
		}
	}

	if err != nil {
		return 0, err
	}
	return count, nil
}

/**
 * ----------------------------------------------------------------------
 * Create Table
 *
 * ----------------------------------------------------------------------
 */
func (query *Query) CreateTable(newTable string, model string) (bool, error) {
	//check transaction and get connection
	_connectionObject := query.Connection

	qry := fmt.Sprintf("CREATE TABLE %s AS TABLE %s", newTable, model)
	query.QueryLog(qry)
	_, err := _connectionObject.Exec(qry)

	if err != nil {
		return false, err
	}
	return true, err
}

/**
 * ----------------------------------------------------------------------
 * Insert fields
 *
 * ----------------------------------------------------------------------
 */
func (query *Query) Insert(tb string, fields map[string]interface{}) (status bool, errR error) {
	//check transaction and get connection
	_connectionObject := query.Connection

	sqlStmt := "INSERT INTO " + tb + " ("
	valueStr := "("
	count := len(fields)
	var i int
	var valArr []interface{}
	for key, val := range fields {
		valArr = append(valArr, val)
		if i++; i < count {
			sqlStmt += key + ","
			switch query.Domine {
			case "mysql":
				valueStr += "?,"
			case "postgres":
				valueStr += fmt.Sprintf("$%d,", i)
			default:
				valueStr += "?,"
			}

		} else {
			sqlStmt += key
			switch query.Domine {
			case "mysql":
				valueStr += "?"
			case "postgres":
				valueStr += fmt.Sprintf("$%d", i)
			default:
				valueStr += "?,"
			}
		}
	}

	sqlStmt += ") "
	valueStr += ")"

	sqlStmt += " VALUES " + valueStr

	query.QueryLog(sqlStmt, fields)
	stmt, err := _connectionObject.Prepare(sqlStmt)

	if err != nil {
		return false, err
	}
	defer stmt.Close()
	res, err := stmt.Exec(valArr...)

	//fmt.Println(res)
	if res != nil {
		return true, nil
	} else {
		return false, err
	}

}

/**
 * ----------------------------------------------------------------------
 * Multi Insert fields
 *
 * ----------------------------------------------------------------------
 */

func (query *Query) MultiInsert(tb string, fields map[int]map[string]interface{}) (status bool, errR error) {
	//check transaction and get connection
	_connectionObject := query.Connection

	sqlStmt := "INSERT INTO " + tb + " ("
	var valArr []interface{}
	var c int
	valueStr := ""
	var i int
	for in, Data := range fields {
		count := len(Data)
		i = 0
		valueStr += "("
		keys := make([]string, 0, len(Data))
		for k := range Data {
			keys = append(keys, k)
		}
		sort.Strings(keys)
		keyL := len(keys)

		for _, k := range keys {
			valArr = append(valArr, Data[k])
			if i++; i < keyL {
				if c == 0 {
					sqlStmt += k + ","
				}
				switch query.Domine {
				case "mysql":
					valueStr += "?,"
				case "postgres":
					valueStr += fmt.Sprintf("$%d,", i+(in*count))
				default:
					valueStr += "?,"
				}

			} else {

				if c == 0 {
					sqlStmt += k
				}
				switch query.Domine {
				case "mysql":
					valueStr += "?"
				case "postgres":
					valueStr += fmt.Sprintf("$%d", i+(in*count))
				default:
					valueStr += "?"
				}
			}
		}
		valueStr += "),"
		c++
	}
	sqlStmt += ") "
	valueStr = valueStr[0 : len(valueStr)-1]

	switch query.Domine {
	case "postgres":
		sqlStmt += " VALUES " + valueStr + " ON CONFLICT DO NOTHING"
	default:
		sqlStmt += " VALUES " + valueStr + ""
	}

	fmt.Println(sqlStmt)
	query.QueryLog(sqlStmt, valArr...)
	stmt, err := _connectionObject.Prepare(sqlStmt)
	if err != nil {
		return false, err
	}
	defer stmt.Close()
	//fmt.Println(valArr)
	res, err := stmt.Exec(valArr...)

	if res != nil {
		return true, nil
	} else {
		return false, err
	}

}

/**
 * ----------------------------------------------------------------------
 * Update
 * ----------------------------------------------------------------------
 */
func (query *Query) Update(tb string, fields map[string]interface{}, cond string, expr string) (status bool, errR error) {

	//check transaction and get connection
	_connectionObject := query.Connection

	sqlStmt := "UPDATE " + tb + " "
	sqlStmt += " SET "
	count := len(fields)
	var i int
	var valArr []interface{}
	domine := query.Domine
	if count > 0 {
		for key, val := range fields {
			valArr = append(valArr, val)
			if i++; i < count {
				switch domine {
				case "mysql":
					sqlStmt += key + " = ?, "
				case "postgres":
					sqlStmt += fmt.Sprintf("%s = $%d,", key, i)
				default:
					sqlStmt += key + " = ?, "
				}
			} else {
				switch domine {
				case "mysql":
					sqlStmt += key + " = ? "
				case "postgres":
					sqlStmt += fmt.Sprintf("%s = $%d", key, i)
				default:
					sqlStmt += key + " = ? "
				}
			}
		}
	}

	if len(expr) > 0 {
		sqlStmt += expr
	}

	if len(cond) > 0 {
		sqlStmt += " WHERE " + cond
	}
	query.QueryLog(sqlStmt, fields)
	stmt, err := _connectionObject.Prepare(sqlStmt)

	if err != nil {
		return false, err
	}
	defer stmt.Close()

	res, err := stmt.Exec(valArr...)

	if res != nil {
		return true, nil
	} else {
		return false, err
	}
}

/**
 * ----------------------------------------------------------------------
 * Delete
 * ----------------------------------------------------------------------
 */
func (query *Query) Delete(tb string, fields map[string]interface{}, cond string) (status bool, err error) {
	//check transaction and get connection
	_connectionObject := query.Connection

	sqlStmt := "DELETE FROM " + tb + " "
	if len(cond) > 0 {
		sqlStmt += " WHERE " + cond
	}

	query.QueryLog(sqlStmt, fields)
	stmt, err := _connectionObject.Prepare(sqlStmt)

	if err != nil {
		return false, err
	}
	defer stmt.Close()

	res, err := stmt.Exec()

	if res != nil {
		return true, nil
	} else {
		return false, err
	}
}
