package go_orm

import (
	"database/sql"
	"fmt"
	"time"

	"gitlab.com/_pratheesh/go-database/adapter"
	"gitlab.com/_pratheesh/go-database/config"
	"gitlab.com/_pratheesh/go-database/query"

	// _ "github.com/go-sql-driver/mysql"
	"gorm.io/driver/mysql"
	"gorm.io/driver/postgres"
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
)

type ORMAdapter struct {
	DBConn                 *sql.DB
	Host, Port, User, Pass string
	Database               string
	Domine                 string
	Sslmode                string

	query.Query
	DBLOG bool

	DB *gorm.DB
}

//
// LoadConfig
//
// Initiate the connection variables
func (db *ORMAdapter) LoadConfig(configVal config.DBConfig) {
	db.Host = configVal.Host
	db.User = configVal.User
	db.Pass = configVal.Pass
	db.Port = configVal.Port
	db.Database = configVal.Database
	db.Domine = configVal.Domine
	db.Sslmode = configVal.Sslmode

}

//
// Close connection
//
func (db *ORMAdapter) DBClose() {
	// db.DB.Close()
}

/**
 * --------------------------------------------------------------------------
 * Initiate database connection
 * --------------------------------------------------------------------------
 */
func (db *ORMAdapter) DBConnection() {
	var connectiondb *gorm.DB
	var err error

	switch db.Domine {
	case "orm_mysql":
		dsn := fmt.Sprintf("%v:%v@tcp(%v:%v)/%v?charset=utf8mb4&parseTime=True&loc=Local", db.User, db.Pass, db.Host, db.Port, db.Database)
		connectiondb, err = gorm.Open(mysql.Open(dsn), &gorm.Config{
			Logger: logger.Default.LogMode(logger.Info),
		})
	case "orm_sqlite":
		connectiondb, err = gorm.Open(sqlite.Open(db.Database+".db"), &gorm.Config{
			Logger: logger.Default.LogMode(logger.Info),
		})
	case "orm_postgres":
		dsn := fmt.Sprintf("host=%v user=%v password=%v port=%v sslmode=%v dbname=%v TimeZone=Asia/Shanghai", db.Host, db.User, db.Pass, db.Port, db.Sslmode, db.Database)

		connectiondb, err = gorm.Open(postgres.Open(dsn), &gorm.Config{
			Logger: logger.Default.LogMode(logger.Info),
		})
	default:
		panic("Unable to find domine ORM")
	}

	if err != nil {
		panic(fmt.Sprintf("failed to connect database %v", err))
	}

	fmt.Println("Connected")
	db.DB = connectiondb

	sqlDB, err := connectiondb.DB()

	// SetMaxIdleConns sets the maximum number of connections in the idle connection pool.
	sqlDB.SetMaxIdleConns(10)

	// SetMaxOpenConns sets the maximum number of open connections to the database.
	sqlDB.SetMaxOpenConns(100)

	// SetConnMaxLifetime sets the maximum amount of time a connection may be reused.
	sqlDB.SetConnMaxLifetime(time.Hour)

}

//
// Connection object
//
func (ormadapter *ORMAdapter) Connection() {
	fmt.Println("Create functon called")

}

func (ormadapter *ORMAdapter) Create() {
	fmt.Printf("%+v", ormadapter)
	fmt.Println("Create functon called")
}

/**
 * ----------------------------------------------------------------------
 * Initiate query
 *
 * Initiate query with null values
 * ----------------------------------------------------------------------
 */
func (dbI *ORMAdapter) QueryInit() adapter.QueryInterface {

	var qinterface adapter.QueryInterface
	return qinterface
}

/**
 * ----------------------------------------------------------------------
 * Clear querys
 *
 * Initiate query with null values
 * ----------------------------------------------------------------------
 */
func (dbI *ORMAdapter) QueryClose() {

}

/**
 * ----------------------------------------------------------------------
 * Log queries
 *
 * ----------------------------------------------------------------------
 */
func (dbI *ORMAdapter) QueryLog(query_stment string, values ...interface{}) {

}
